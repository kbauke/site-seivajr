import React from 'react';
import Layout from '../components/layout/Layout';

const NotFound = () => (
  <Layout>
    <h1>Página não encontrada!</h1>
    <p>Você entrou em uma página que apenas não existe :(</p>
  </Layout>
);

export default NotFound;
