const Tag = ({ children }) => {
  return (
    <small className="mr-2 bg-primary-lighter text-white p-1 rounded-md">{children}</small>
  );
};

export default Tag;
